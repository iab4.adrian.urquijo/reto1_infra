# README - Repositorio de Nodered, InfluxDB y Grafana

Configuración lista de Nodered, InfluxDB y Grafana en Docker, junto con un script en Python para generar datos y un archivo JSON para importar en Nodered, el cual usamos para generar otro apartado de los datos.

## Instalación

1. Clona el repositorio en tu máquina local:

    git clone https://github.com/https://gitlab.com/iab4.adrian.urquijo/reto1_infra.git

2. Ve al directorio clonado:

    cd reto1_infra

3. Para construir las imágenes Docker, ejecuta:

    docker-compose build

4. Una vez que las imágenes se hayan construido con éxito, inicia los contenedores:

    docker-compose up -d

### Generar Datos con Python

El script Python  de Juptyer Notebook en este repositorio te permite generar datos que se mostrarán en Grafana a través de InfluxDB. Para ejecutar el script, asegúrate de tener Python instalado y ejecutarlo desde tu editor de codigo.

### Importar Datos en Nodered

1. Accede a Nodered en tu navegador web: [http://localhost:1880](http://localhost:1880).

2. Importa el archivo JSON `nodered_flow.json` para cargar la configuración de Nodered que generará datos adicionales.

### Acceso a las Aplicaciones

- Nodered: [http://localhost:1880](http://localhost:1880)
- Grafana: [http://localhost:3000](http://localhost:3000)
- InfluxDB: [http://localhost:8086](http://localhost:8086)

## Detener y Eliminar Contenedores

Para detener y eliminar los contenedores creados por Docker Compose, ejecuta:

docker-compose down

# Correciones

## Script Bash
Hemos creado un Script de bash para automatizar la creacion de 3 distintos usuarios.

## Python
Hemos modificado el codigo para que todos los registros que se suban a InfluxDB tengan un 'tag' asociado. 
Todos estos registros se suben linea a linea.

## Problemas
No hemos sido capaces de crear un token de influxdb mediante docker.
La frecuencia de carga de datos en influxdb estaba definida a dias, por lo cual no eramos capaces de ver si los datos estaban subiendo correctamente.
Hemos tenido que subir todos los csv de nuevo con un tag asociado para que los datos no se separasen.
