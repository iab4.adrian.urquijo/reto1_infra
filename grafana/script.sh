#!/bin/bash

# URL de tu servidor Grafana
# Generamos con las claves de admin de grafana, ya que el tokenApi no funciona
GRAFANA_URL='http://admin:admin@localhost:3000'

# Lista de usuarios a crear
declare -a usuarios=("adrian" "rodrigo" "pepito")

# Contraseña para los nuevos usuarios
password="1234"

# Recorre la lista de usuarios
for usuario in "${usuarios[@]}"
do
   echo "Creando usuario $usuario"
   
   # Comando para crear un usuario en Grafana
   curl -X POST -H "Content-Type: application/json" \
        -d '{
              "name":"'$usuario'",
              "email":"'$usuario'@example.com",
              "login":"'$usuario'",
              "password":"'$password'"
            }' \
        $GRAFANA_URL/api/admin/users
   
   echo "Usuario $usuario creado con éxito"
done